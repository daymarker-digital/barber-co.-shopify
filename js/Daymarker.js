// @codekit-prepend quiet "../node_modules/shopify-cartjs/dist/rivets-cart.min.js";
// @codekit-prepend quiet "../node_modules/momentsjs/min/moment-with-locales.min.js";

///////////////////////////////////////////////////////////////////////////////////////////////
////  Vendor
///////////////////////////////////////////////////////////////////////////////////////////////

// @codekit-prepend quiet "./vendors/_bootstrap--3.3.7.js";
// @codekit-prepend quiet "./vendors/_elementQueries.js";
// @codekit-prepend quiet "./vendors/_jQueryUI--1.12.1.js";
// @codekit-prepend quiet "./vendors/_jQueryValidate--1.17.0.js";
// @codekit-prepend quiet "./vendors/_lodash--4.17.10.js";
// @codekit-prepend quiet "./vendors/_microModal--0.4.2.js";
// @codekit-prepend quiet "./vendors/_owlCarousel--2.2.1.js";
// @codekit-prepend quiet "./vendors/_resizeSensor.js";
// @codekit-prepend quiet "./vendors/_scrollTto--2.1.2.js";

//////////////////////////////////////////////////////////
////  Credits
//////////////////////////////////////////////////////////

var Credits = (function () {
  
	var showCredits = true;
	var info = { company : 'Barber & Co.', tagline : 'Grooming Line for the Modern Gentleman',  version : 2.9 };
  	  
	//////////////////////////////////////////////////////////
	////  Public Methods
	//////////////////////////////////////////////////////////
  
	var init = function () {
		if ( showCredits ) {
			console.log( info.company + ' - ' + info.tagline + ' - Version ' + info.version );  
			console.log( 'Site by Daymarker Digital ( https://www.daymarker.digital )' );
    	}
	};
	
	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////
	
	return {
		init : init
	};

})();

//////////////////////////////////////////////////////////
////  Utilities
//////////////////////////////////////////////////////////

var Utilities = (function () {
	
	var debugThis = true;
	var info = { name : 'Utilities', version : 1.0 };
		
	//////////////////////////////////////////////////////////
	////  Public | Is Object Empty
	//////////////////////////////////////////////////////////
	
	var isEmpty = function( $obj ) {
			
		for ( var prop in $obj ) {
		
			if ( $obj.hasOwnProperty( prop ) ) {
				return false; 
		    }
		
		}
		
		return JSON.stringify( $obj ) === JSON.stringify( {} );
			
	};
	
	//////////////////////////////////////////////////////////
	////  Public | Get All Cookies
	//////////////////////////////////////////////////////////
	
	var getAllCookies = function() {
		
		console.log( '[ getAllCookies() ] Initialized' );
		console.log( document.cookie );
		console.log( '[ getAllCookies() ] Complete' );			
		
	};
	
	//////////////////////////////////////////////////////////
	////  Public | Get Cookie
	//////////////////////////////////////////////////////////
	
	var getCookie = function( $cookie_name ) {
		
		var nameEQ = $cookie_name + "=";
		var ca = document.cookie.split(';');
		
		for ( var i=0; i < ca.length; i++ ) {
			var c = ca[i];
			while ( c.charAt(0)==' ' ) c = c.substring( 1, c.length );
			if ( c.indexOf( nameEQ ) == 0 ) return c.substring( nameEQ.length, c.length );
		}
		
		return null;
		
	};
	
	//////////////////////////////////////////////////////////
	////  Public | Set Cookie
	//////////////////////////////////////////////////////////
	
	var setCookie = function( $cookie_name, $cookie_value, $days_until_expired ) {
		
		var expires;
				
		if ( $days_until_expired ) {
			
			var date = new Date();
			date.setTime( date.getTime() + ( $days_until_expired*24*60*60*1000 ) );
			expires = "; expires=" + date.toGMTString();
			
		} else {
			expires = "";
		}
						
		document.cookie = $cookie_name + "=" + $cookie_value + expires + "; path=/";
					
	};
	
	//////////////////////////////////////////////////////////
	////  Public | Remove Cookie
	//////////////////////////////////////////////////////////
	
	var removeCookie = function( $cookie_name ) {			
		document.cookie = $cookie_name + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/;";
	};
		
	//////////////////////////////////////////////////////////
	////  Returned Methods
	//////////////////////////////////////////////////////////
	
	return {
		info : info,
		isEmpty : isEmpty,
		getAllCookies : getAllCookies,
		getCookie : getCookie,
		setCookie : setCookie,
		removeCookie : removeCookie
	};
	
})();

//////////////////////////////////////////////////////////
////  Debugging
//////////////////////////////////////////////////////////

var Debug = (function () {
  
	var info = { name : 'Debug', version : 1.0 };
  
	//////////////////////////////////////////////////////////
	////  Public Methods
	//////////////////////////////////////////////////////////
  
	var log = function ( $enable, $info, $status ) {
      	
      	var message = 'Status Not Set';
      	var infoName = 'Default Name';
      	var infoVersion = 'Default Version';
      
		if ( $enable ) {
          
			if ( !_.isEmpty( $info ) ) {
				infoName = $info.name;
				infoVersion = $info.version;
			}
          
			switch ( $status ) {
            	case 'start':
					message = '[ ' + infoName + ' ] Initialized';         
                	break;
                case 'stop':
                    message = '[ ' + infoName + ' ] Completed v.' + infoVersion;
                break;
            } // end switch   
          
        	console.log( message );
		}
            
	};
  
	var thisVar = function ( $mixedVar ) {
      
		if ( _.isEmpty( $mixedVar ) ) {
			console.log( '[ Debug.thisVar() ] Error : $mixedVar is null or empty' );
		} else {
			console.log( '[ Debug.thisVar() ] $mixedVar is type: ' + typeof $mixedVar );
        	console.log( JSON.stringify( $mixedVar, null, 2) ); 
        }
  
	};
	
	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////
	
	return {
		info : info,
		log : log,
		thisVar : thisVar
	};

})();

//////////////////////////////////////////////////////////
////  Viewport
//////////////////////////////////////////////////////////

var Viewport = (function () {
  
  	var info = { name : 'Viewport', version : 1.0 };
  
  	//////////////////////////////////////////////////////////
	////  Public | On Resize
	//////////////////////////////////////////////////////////
  
	var onResize = function ( $function, $delay ) {
      	
		var timer; // Set resizeTimer to empty so it resets on page load
	
	    // On resize, run the $function and reset the timeout
	    // $delay in milliseconds. Change as you see fit.
	    
	    $(window).on('resize', function() {
		    
	        clearTimeout( timer );
	        
	        timer = setTimeout( $function, $delay );
	        
	    });
	
	    $function;      
      	            
	};
	
	//////////////////////////////////////////////////////////
	////  Public | On Scroll
	//////////////////////////////////////////////////////////
	
	var onScroll = function ( $function, $delay ) {
      	
		var timer; // Set resizeTimer to empty so it resets on page load
	
	    // On resize, run the $function and reset the timeout
	    // $delay in milliseconds. Change as you see fit.
	    
	    $(window).on('scroll', function() {
		    
	        clearTimeout( timer );
	        
	        timer = setTimeout( $function, $delay );
	        
	    });
	
	    $function;      
      	            
	};
	
	//////////////////////////////////////////////////////////
	////  Public | On Scroll
	//////////////////////////////////////////////////////////
	
	var _offsetWatcher = function() {
		
		var scroll;
		var offsetRange = 7;
			
		setState();
		
		$(window).on( "scroll", function() {
			setState();
		});
		
		function setState() {
					
			scroll = $(window).scrollTop();
					
			// console.log( 'scroll height: ' + scroll );
					
			if ( scroll >= offsetRange ) {
				// below the top of viewport
				$("header").removeClass('at-top');
				$("header").addClass('offset-from-top');
			} else {
				$("header").addClass('at-top');
				$("header").removeClass('offset-from-top');
				// at top of viewport
			}    				
				
		}	
						
	};
  
	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////
	
	return {
		onResize : onResize,
		onScroll : onScroll
	};

})();

///////////////////////////////////////////////////////////////////////////////////////////////
////  Load Scripts | Daymarker	
///////////////////////////////////////////////////////////////////////////////////////////////

// @codekit-prepend "./modules/_carousels.js";
// @codekit-prepend "./modules/_loading-screen.js";
// @codekit-prepend "./modules/_main-menu.js";
// @codekit-prepend "./modules/_products.js";
// @codekit-prepend "./modules/_push-content.js";
// @codekit-prepend "./modules/_shopping-cart.js";
// @codekit-prepend "./modules/_viewport-actions.js";

//////////////////////////////////////////////////////////////////////////////////////////
////  Execute Scripts!
//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////
////  Immediately
//////////////////////////////////////////////////////////

$(function(){

	// Site Credits
	Credits.init();
			
	// Initialize CartJS
	CartJS.init( StoreInfo.cart, { "dataAPI": true, "debug": false, "requestBodyClass": "cart-loading" } );
		
}());
		
//////////////////////////////////////////////////////////
////  When 'document' is ready
//////////////////////////////////////////////////////////		
		
$(document).ready(function(){
	
	// Background Videos Function			
	BackgroundVideos.init();	
	
	// Loading Screen Function
	LoadingScreen.init();	
	
	// Viewport Actions
	ViewportActions.init();
	
	// Carousels Function
	Carousels.init();
	
	// Main Menu Function
	MainMenu.init();
	
	// Push Content Function
	PushContent.init();
	
	// Shopping Cart Function
	ShoppingCart.init();
	
	// Products Function
	Products.init();
	
});

//////////////////////////////////////////////////////////
////  When 'cart' is ready
//////////////////////////////////////////////////////////


$(document).on('cart.ready', function( event, cart ) {
  
  let debug = true;

  if ( debug ) {
    console.log( '[ CartJS() ] cart.ready' );
    console.log( 'event:' );
    console.log( event );
    console.log( 'cart:' );
    console.log( cart );
    console.log( '---------' );
  }  

});

//////////////////////////////////////////////////////////
////  When 'cart request' started
//////////////////////////////////////////////////////////

$(document).on('cart.requestStarted', function(event, cart) {
  
  let debug = true;

  if ( debug ) {
    console.log( '[ CartJS() ] cart.requestStarted' );
    console.log( 'event:' );
    console.log( event );
    console.log( 'cart:' );
    console.log( cart );
    console.log( '---------' );
  }  
  
});

//////////////////////////////////////////////////////////
////  When 'cart request' is complete
//////////////////////////////////////////////////////////

$(document).on( 'cart.requestComplete', function( event, cart ) {

  let debug = true;

  if ( debug ) {
    console.log( '[ CartJS() ] cart.requestComplete' );
    console.log( 'event:' );
    console.log( event );
    console.log( 'cart:' );
    console.log( cart );
    console.log( '---------' );
  }
  
  ShoppingCart.update( cart );

});

