//////////////////////////////////////////////////////////
////  Carousels
//////////////////////////////////////////////////////////

var Carousels = (function () {

	var debugThis = false;
	var info = { name : 'Carousels', version : 3.6969 };
	var containerClass = '.carousel-container';
	var navigationClasses = {
		next : '.carousel-controls--next, .carousel-nav--next, .carousel-nav.next, .carousel-nav--next',
		prev : '.carousel-controls--prev, .carousel-nav--prev, .carousel-nav.prev, .carousel-nav--prev'
	};
 
	//////////////////////////////////////////////////////////
	////  Private Methods
	//////////////////////////////////////////////////////////
	
	var owlCarousels = function () {
		
		var target = $( containerClass );
		var carousels = [];

		// iterate over each carousel element
		target.each(function(){
			
			if ( $(this).find('.owl-carousel').length ) {
				
				var thisEl = $(this);
				var id = thisEl.attr( 'id' );
				var carousel = $( '#' + id + ' .owl-carousel' );
				var settings = {
					autoplay: 0,
					margin: 0,
					loop: true,
					nav: 0,
					dots: true,
					autoplayHoverPause: 1,
					smartSpeed: 450,
					autoplayTimeout: 3250,
					autoHeight: 0,
					items: 1
				};
				
				// add custom settings based on class modifier
				if ( $(this).hasClass('carousel-container--past-projects') ) {
		        	settings.loop = 1;
			        settings.center = 1;	
			        settings.items = 5;	
			        settings.autoplay = 1;
			        settings.autoplayTimeout = 1750;
			        settings.autoplayHoverPause = 1;
			        settings.autoplaySpeed = 550;
			        settings.autoWidth = 1;
			        settings.center = 1; 
			        settings.margin = 0;
			        settings.dots = 0;
			        settings.dotsContainer = '.carousel__dots';
	            }
	            
	            if ( $(this).hasClass('carousel-container--journal') ) {
		        	settings.loop = 1;
			        settings.center = 1;	
			        settings.items = 1;	
			        settings.autoHeight = 1;
			        settings.autoWidth = 0;
			        settings.center = 1; 
			        settings.margin = 0;
			        settings.dots = 1;
			        settings.onInitialized = refreshMe;
					settings.onResized = refreshMe;
	            }
				
				// add carousel to array
				carousels.push({
					id : id,
					carousel : carousel,
					settings : settings
				});
				
			}
						
		});
		
		//////////////////////////////////////////////////////////
		////  Carousel Initialization & Setup
		//////////////////////////////////////////////////////////
		
		$.each( carousels, function( index, item ) {
			
			
			item.carousel.on( 'initialize.owl.carousel initialized.owl.carousel', function( event ) {
				console.log( event );
			});
			
			
			item.carousel.owlCarousel( item.settings );
			
		});
		
		//////////////////////////////////////////////////////////
		////  Carousel Navigation
		//////////////////////////////////////////////////////////
		
		$('.carousel__dot').on('click', function(event) {
			var carouselID = $(this).closest( containerClass ).attr('id');
			$("#" + carouselID + " .owl-carousel").trigger('to.owl.carousel', [ $(this).index(), 280 ] );
		});
		
		$( navigationClasses.prev ).on("click", function(event) {
            var carouselID = $(this).closest( containerClass ).attr('id');
            $("#" + carouselID + " .owl-carousel").trigger('prev.owl.carousel', [280]);
        });

        $( navigationClasses.next ).on("click", function(event) {
            var carouselID = $(this).closest( containerClass ).attr('id');
            $("#" + carouselID + " .owl-carousel").trigger('next.owl.carousel', [280]);
        });
        
        //////////////////////////////////////////////////////////
		////  Carousel Callback
		//////////////////////////////////////////////////////////
                
        function refreshMe ( event ) {
	        
	        console.log( event );
	        
	        var thisClasses = event.target.offsetParent.className;
	        var thisId = event.target.offsetParent.id;
	        
	        $("#" + thisId + " .owl-carousel").find('.carousel__item').removeClass('refreshed');
	        
	        setTimeout( function(){
		        
		        $("#" + thisId).find('.carousel__loading-screen').removeClass('active');
				$("#" + thisId + " .owl-carousel").trigger('refresh.owl.carousel');
				$("#" + thisId + " .owl-carousel").find('.carousel__item').addClass('refreshed');
				$("#" + thisId).find('.carousel-nav').addClass('refreshed');
				
			}, 500 );			
	       	        
        }
		
	};

	//////////////////////////////////////////////////////////
	////  Public Method | Initialize
	//////////////////////////////////////////////////////////

	var init = function() {
		
		Debug.log( debugThis, info, 'start' );
				
		owlCarousels();
		
		Debug.log( debugThis, info, 'stop' );
		
	};
	
	//////////////////////////////////////////////////////////
	////  Returned Methods
	//////////////////////////////////////////////////////////
	
	return {
		info : info,
		init : init
	};

})();