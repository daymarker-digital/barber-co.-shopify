//////////////////////////////////////////////////////////
////  Offset Watcher
//////////////////////////////////////////////////////////

var OffsetWatcher = (function () {
	
	var debugThis = false;
	var info = { name : 'OffsetWatcher', version : 1.0 };
	
	//////////////////////////////////////////////////////////
	////  Private Methods
	//////////////////////////////////////////////////////////
		
	var _setOffsetState = function ( $element ) {
		
		if ( $element.length ) {
			
			var scroll = $(window).scrollTop();
		
			if ( scroll >= 5 ) {
				$element.removeClass('at-top').addClass("offset-from-top");
			} else {
				$element.removeClass("offset-from-top").addClass('at-top');
			}    
				
		}
      		  
	};

	//////////////////////////////////////////////////////////
	////  Public Methods
	//////////////////////////////////////////////////////////
	
	var offsetHeader = function () {
		
		var target = $("header");
		
		_setOffsetState( target );
		
		$(window).on( "scroll", function() {
			_setOffsetState( target );
		});
		
	};
			
	var init = function( $params ) {
      
		Debug.log( debugThis, info, 'start' );
      
		offsetHeader();
      
		Debug.log( debugThis, info, 'stop' );	
      
	};
	
	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////
	
	return {
		info : info,
		init : init
	};
	
})();