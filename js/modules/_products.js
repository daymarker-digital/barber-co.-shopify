//////////////////////////////////////////////////////////
////  Products
//////////////////////////////////////////////////////////

var Products = (function () {
	
	var debugThis = false;
	var info = { name : 'Products', version : 2.6 };
	var pushContentCloseTimeout = 2000;
		
	//////////////////////////////////////////////////////////
	////  Private Methods
	//////////////////////////////////////////////////////////
		
	var _richTextModifier = function () {
      
		var productDescContainer = $("#product-description");
		var productDescExtrasContainer = $("#product-description-extras");
		var firstChildEl, firstChildElContent, firstChildElTag;
		var nextChildEl, nextChildElContent, nextChildElTag;
		var contentHeadingEl, contentHeadingElContent, extraContentHeadingElTag;
		var arrOfProductDescElements = [];
		var arrOfProductDescElExtras = [];
		var count;

		if ( productDescContainer.length && productDescExtrasContainer.length ) {
          
			//////////////////////////////////////////////////////////
            ////  Get & Store Main Content from Product Desc
            //////////////////////////////////////////////////////////
          	
			contentHeadingEl = productDescContainer.find("h2");
          	
			if ( contentHeadingEl.length ) {

				count = 0;
              
                contentHeadingEl.each(function() {
                  
                  	// Get content of 'h2'
                	contentHeadingElContent = $(this).html();
                  	// Add content object to 'arrOfProductDescElements' array
                  	arrOfProductDescElements[count] = { headingContent : contentHeadingElContent, extraContent : [] };
                  
                  	var i = 0;
                  	var elementTag = "";
                  	var elementSrc = "";
                  	var elementAltText = "";
                  	var elementContent = "";
                  	var elementObject;
                  
                  	$(this).nextUntil("h2").each(function(){ 

                        elementTag = $(this).prop("tagName").toLowerCase();
                      
                        if ( "img" === elementTag ) {
                            elementSrc = $(this).prop("src");
                            elementAltText = $(this).prop("alt");
                        }
                      
                        elementContent = $(this).html(); 
                        elementObject = { tag : elementTag, contents : elementContent, imgSrc : elementSrc, imgAltText : elementAltText };

                        arrOfProductDescElements[count].extraContent[i] = elementObject;	

                        i++;
                        $(this).remove(); // removes 'this' from 'dom'

                    }); // each 'h2'

                    count++;
                    $(this).remove();

                }); // each
              
				if ( debugThis ) {
					console.log( JSON.stringify( arrOfProductDescElements, null, 2 ) );
				}
              
            } // end if
          
            //////////////////////////////////////////////////////////
            ////  Set Tabs Navigation from 'arrOfProductDescElements'
            //////////////////////////////////////////////////////////
          
          	if ( arrOfProductDescElements.length ) {
              
              	var html = "";
              	var activeState;
              
              	html = "<ul class='tabs-navigation'>";

                for ( var i = 0; i < arrOfProductDescElements.length; i++ ) {

                  	var tagHeading = arrOfProductDescElements[i].headingContent;
                  	
                  	if ( i === 0 ) {
                      	activeState = "active";
                    } else {
                    	activeState = "";
                    }
                  
                  	html += "<li class='tabs-navigation-item " + activeState + "'>" + tagHeading + "</li>";
                  
                } // end for loop
              
              	html += "</ul>"
              
              	productDescExtrasContainer.append( html );

            } else {
            	// No Product Description Content - Do Nothing!
            }
          
          	//////////////////////////////////////////////////////////
            ////  Set Tabs Content from 'arrOfProductDescElements'
            //////////////////////////////////////////////////////////
          
            if ( arrOfProductDescElements.length ) {

				var html = "";
              	var elementTag, elementContent, elementSrc, elementAltText;
            	
              	html = "<ul class='tabs-content'>";
              
                // Populate 'Extra Features' content area
                for ( var i = 0; i < arrOfProductDescElements.length; i++ ) {
                  
                  	html += "<li class='tabs-content-item'>";
                  	html += "<div class='tab-content-padding'>";
                  
                  	// Content Goes Here
                  	for ( var k = 0; k < arrOfProductDescElements[i].extraContent.length; k++ ) { 

                        elementTag = arrOfProductDescElements[i].extraContent[k].tag;
                        elementContent = arrOfProductDescElements[i].extraContent[k].contents;

                      	if ( "img" === elementTag ) {
                            elementSrc = arrOfProductDescElements[i].extraContent[k].imgSrc;
                            elementAltText = arrOfProductDescElements[i].extraContent[k].imgAltText;
                            html += "<" + elementTag + " src='" + elementSrc + "' alt='" + elementAltText + "' />";
                        } else {
                            html += "<" + elementTag + ">";
                            html +=  elementContent;
                            html += "</" + elementTag + ">";    
                        }

                    } // end for 'inner'
                  
                  	html += "</div>";
                  	html += "</li>";
                
                } // end for
              
              	html += "</ul>";
              
              	productDescExtrasContainer.append( html );

            } // if array of content AND target container exist
          	
        }
				      
	};
	
	var _infotabsControllers = function() {
  	
      	$("#product-description-extras").on("click", ".tabs-navigation-item", function(event){
                    
          	$(".tabs-navigation-item").removeClass("active");
          
          	$(this).addClass("active");
      		
          	var index = $(this).index();
          
          	$(".tabs-content li").hide();
          	
          	$(this).closest(".tabs-content-container").find(".tabs-content li").eq( index ).fadeIn(500);
          
      	});
      
    };  
	
	var _infoPositioning = function() {
		
		setPosition();
      
		new ResizeSensor( jQuery('#feature-product-image-carousel'), function() {
			setTimeout(function(){
				setPosition();
			}, 250);
		});
      
		function setPosition() {
          	
			var matchParent, matchChild;
			var matchContainer = $(".js-match--container");
			matchParent = matchContainer.find(".js-match--parent");
			matchChild = matchContainer.find(".js-match--child");
			var referenceHeight;
          
			if ( window.innerWidth >= 992 ) {
                    
                referenceHeight = matchParent.outerHeight();                
                matchChild.css({
                    "height": referenceHeight + "px"
                });

            } else {
                matchChild.css({
                    "height":"auto"
                });
            }
                
        };
		
	};
	
	var _variantOptionControllers = function() {
    
      	$(".product-info__cta").on("click", ".variants-list-item--option", function(event){
          
          	// Remove 'selected' state from option list
            $(".variants-list-item--option").removeClass("selected");
          
          	if ( $(this).hasClass("not-available") ) {
    			$("input[name='variantID']").val(0);      
            } else {
              
                // Set 'selected' state on option list(s)
                var index = $(this).index();
                $(".variants-list").each(function(){
                    $(this).find("li").eq(index).addClass("selected");	
                });

                var variantDataObject = $(this).data("variantInfo");
                var id = variantDataObject.ID;
                var available = variantDataObject.available;
                var sku = variantDataObject.sku;
                var thisOption = variantDataObject.name;
                var inventory = variantDataObject.inventory
                var thisOptionPrice = parseInt( variantDataObject.price / 100 ).toFixed(2);

                if ( available ) {
                    $("input[name='variantID']").val(id);
                } else {
                    $("input[name='variantID']").val(0);
                }  

                $(".product-price--amount").html(thisOptionPrice);
              
            } // end if else
          
        });
      
    };
	
	var _addToCartFromCollection = function() {
       
       	var timer;
       
        $(".collection-item-cta").on("click", ".collection-item-cta__variant-list-item", function(event){
          	
          	$(this).closest(".collection-item-cta__variant-list").find(".collection-item-cta__variant-list-item").removeClass("selected");
                    
          	if ( $(this).hasClass("not-available") ) {
            	$(this).closest(".collection-item-cta").find("input[name='variantID']").val(0);
            } else {
          		$(this).addClass("selected");
            	var variantID = parseInt( $(this).data("variantId") );
              	$(this).closest(".collection-item-cta").find("input[name='variantID']").val(variantID);
            }
        
        });
      
        $(".collection-button--add-to-cart").on("click", function(event){
          
          	var variantID = parseInt( $(this).closest(".collection-item-cta").find("input[name='variantID']").val() );
          	
          	if ( variantID ) {
              
              	CartJS.addItem( variantID, 1, {}, {
					"success": function(data, textStatus, jqXHR) {   
                      
                      	$(".push-content--cart-summary").addClass("pushed-right");
                      	clearTimeout(timer);  
                      	timer = setTimeout(function(){
                           $(".push-content--cart-summary").removeClass("pushed-right");
                        }, pushContentCloseTimeout);  
                      
                   	},
                   	"error": function(jqXHR, textStatus, errorThrown) {
                    }	
                  
              	}); // CartJS.addItem()  
          
            } else {
            
              	// Prompt user to valid option
              
            }

        });
    
    };
	
	var _addToCart = function() {
      
      	var timer;
  
      	$(".product-button--add-to-cart").on("click", function(event){
                    
          	var variantID = parseInt( $("input[name='variantID']").val() );
          
          	if ( variantID ) {
              
              	CartJS.addItem( variantID, 1, {}, {

					"success": function(data, textStatus, jqXHR) {
                      
                      	$(".push-content--cart-summary").addClass("pushed-right");
                      	clearTimeout(timer);  
                      	timer = setTimeout(function(){
                           $(".push-content--cart-summary").removeClass("pushed-right");
                        }, pushContentCloseTimeout);  
                      
                   	},

                   	"error": function(jqXHR, textStatus, errorThrown) {
                      
                    	console.log("addToCart() : CartJS.addItem() : error! ErrorThrown: " + errorThrown + "! TextStatus: " + textStatus + " jqXHR: " + jqXHR);
                      	console.log( console.log( JSON.stringify(jqXHR, null, 2) ) );
                      
                      	if ( jqXHR.status === 422 ) {
                      		alert("You have all variants in your cart!");
                        }
                      
					}

              	}); // CartJS.addItem()  
                            	
            } else {
            	// Throw error!
              	console.log("No variant ID to add to cart!!!");
              	$(".error-message--no-option-selected").fadeIn(550, function(){
              		// callback
                });
            }
              
      	});
            
    };
	
	var _galleries = function() {
		
		// Vars
		
		var headerHeight = $('header').outerHeight();
		var viewportHeight = window.innerHeight;
		var viewportWidth = window.innerWidth;
		
		var galleryHeights = {
			'default' : '280px',
			'extra-small' : '360px',
			'small' : '420px',
			'medium' : '500px',
			'large' : '600px'
		};
		
		var elementsToSizeArr = {
			product : [
				'#product .product__info--desktop', 
				'#product .product__gallery--main',
				'#product .product__gallery--main .image',
				'#product .product__gallery--main .background-video'
			],
			lifestyle : [
				'#product .product__gallery--lifestyle',
				'#product .product__gallery--lifestyle .background-image',
				'#product .product__gallery--lifestyle .background-video'
			]
		} 
		
		var productElementsToSize = elementsToSizeArr.product.join();
		var lifestyleElementsToSize = elementsToSizeArr.lifestyle.join();
				
		// Gallery 'Product' Conditions	
				
		if ( viewportWidth >= 992 ) {
			
			if ( viewportWidth >= 1200 ) {
				
				if ( viewportHeight > ( 600 + headerHeight ) ) {
					$( productElementsToSize ).css({
						"height" : ( viewportHeight - headerHeight ) + "px"
					});
				} else {
					$( productElementsToSize ).css({
						"height" : "600px"
					});
				}
				
			} else {
				
				if ( viewportHeight > ( 500 + headerHeight ) ) {
					$( productElementsToSize ).css({
						"height" : ( viewportHeight - headerHeight ) + "px"
					});
				} else {
					$( productElementsToSize ).css({
						"height" : "500px"
					});
				}	
				
			}					
			
		} else {
			$( productElementsToSize ).removeAttr('style');
		}
		
		// Gallery 'Lifestyle' Conditions
		
		$( lifestyleElementsToSize ).css({
			"height" : viewportHeight + "px"
		});
				
	};
	
	//////////////////////////////////////////////////////////
	////  Public Methods
	//////////////////////////////////////////////////////////
			
	var init = function( $params ) {
      
		Debug.log( debugThis, info, 'start' );
		
		if ( $("#product").length ) {
			
			_richTextModifier();
			_infotabsControllers();	
			_galleries();
			
			$(window).on('resize',function(){
				_galleries();
			});
			
		}		
		
		_variantOptionControllers();
		_addToCart();
		_addToCartFromCollection();
            	
		Debug.log( debugThis, info, 'stop' );	
      
	};
	
	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////
	
	return {
      	info : info,
		init : init
	};	

})();