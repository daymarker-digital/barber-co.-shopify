var ShoppingCart = (function () {
	
	var debugThis = false;
	var info = { name : 'ShoppingCart', version : 1.0 };
	
	//////////////////////////////////////////////////////////
	////  Private Methods
	//////////////////////////////////////////////////////////
		
	var _updateLineItem = function() {
	      
      	var cartItemsTotal, currentLineItem, thisVariantID, currentInventory, currentQuantity, newQuantity, updateOption;
          
      	$("#push-cart-list").on("click", ".js--update-cart", function(event){
          
          	event.preventDefault();
          
          	// get update option
          	updateOption = $(this);
          	
          	// get 'clicked' line item
      		currentLineItem = $(this).closest("li.push-cart-list-item");
          	
      		// get variant info from line item
      		thisVariantID = parseInt( currentLineItem.data("variantId") );
          
          	//currentInventory = parseInt( currentLineItem.data("variantInventory") );
            
			// get quantity of line item
      		currentQuantity = parseInt( currentLineItem.find("input.cart-list-item__quantity").val() );
          
          	//////////////////////////////////////////////////////////
            ////  Remove Line Item
            //////////////////////////////////////////////////////////
          
          	if ( updateOption.hasClass("remove") ) {
            
            	removeLineItem( thisVariantID, currentLineItem );
              
            } // if option is 'remove' 
          
          	//////////////////////////////////////////////////////////
            ////  Decrease Line Item Quantity
            //////////////////////////////////////////////////////////
          
          	if ( updateOption.hasClass("decrease") ) { 
              
              	newQuantity = currentQuantity - 1;
              
              	if ( newQuantity === 0 ) {
              		
                  	removeLineItem( thisVariantID, currentLineItem );
                  
                } else {
                
                  	updateLineItem( thisVariantID, newQuantity, currentLineItem );
                
                }
          
            } // if option is 'decrease'
          
          	//////////////////////////////////////////////////////////
            ////  Increase Line Item Quantity
            //////////////////////////////////////////////////////////
          
          	if ( updateOption.hasClass("increase") ) { 
              
              	newQuantity = currentQuantity + 1;
              	
              	updateLineItem( thisVariantID, newQuantity, currentLineItem );
              
            } // if option is 'increase'
          
        }); // click
      
      	//////////////////////////////////////////////////////////
        ////  Nested Function - Update Line Item
        //////////////////////////////////////////////////////////
      
      	function updateLineItem( $variantID, $quantity, $thisLineItem ) {
          
              	CartJS.updateItemById( $variantID, $quantity, {}, {
            		"success": function(data, textStatus, jqXHR) {
                   		//$thisLineItem.find("input.cart-list-item__quantity").val($quantity);
                        //$thisLineItem.find(".product-quantity-list-item--quantity").html($quantity);
                      	if ( debugThis ) {
                        	console.log( "[ shoppingCart() ] Successfully increased variant " + $variantID + " to " + $quantity );
                        }
                  	},
                   	"error": function(jqXHR, textStatus, errorThrown) {
                      	if ( debugThis ) {
                            console.log( "[ shoppingCart() ] Error: " + errorThrown + "!" );
                            console.log( JSON.stringify( jqXHR, null, 2 ) );
                        }
                  	}
             	}); // CartJS.updateItemById()
          
        } // function updateLineItem()
      
      	//////////////////////////////////////////////////////////
        ////  Nested Function - Remove Line Item
        //////////////////////////////////////////////////////////
      
      	function removeLineItem( $variantID, $thisLineItem ) {
          	
              	CartJS.removeItemById( $variantID, {
                	"success": function(data, textStatus, jqXHR) {
                        $thisLineItem.fadeOut(320, function(){
							// Cart is empty
                            setTimeout(function(){
                                $thisLineItem.remove();
                            }, 150);	
                        });
                        if ( debugThis ) {
                          	console.log( "[ shoppingCart() ] Successfully removed variant " + $variantID + " from the cart" ); 
                        }
               		},
               		"error": function(jqXHR, textStatus, errorThrown) {
                      	if ( debugThis ) {
                          	console.log( "[ shoppingCart() ] Error: " + errorThrown + "!" );
                          	console.log( JSON.stringify( jqXHR, null, 2 ) );
                        }
                  	}
              	}); // CartJS.removeItemById()
          
        } // function updateLineItem()
     
    }; // upateLineItem()
	
	//////////////////////////////////////////////////////////
	////  Public Methods
	//////////////////////////////////////////////////////////
			
	var init = function( $params ) {
      
		Debug.log( debugThis, info, 'start' );
      
		_updateLineItem();
            
		Debug.log( debugThis, info, 'stop' );	
      
	};
	
	var update = function( $cart ) {
        
		var arrOfLineItems = $cart.items;
		var cartTotalItemsCount = $cart.item_count;
		var cartTotalPrice = ($cart.original_total_price/100).toFixed(2);
		var shopCurrency = StoreInfo.currency;
		var imageFileTypes = [ ".png", ".jpg", ".jpeg", ".gif"  ];
		var iconQtyDecrease = StoreInfo.assets.icons.qty_decrease;
		var iconQtyIncrease = StoreInfo.assets.icons.qty_increase;
		var itemsUpdated = {};
  
		$(".push-cart-note--amount").html("$" + cartTotalPrice + " " + shopCurrency );
		$(".total-cart-items").html( cartTotalItemsCount );
  
		if ( cartTotalItemsCount > 0 ) {
	  
			// At first, we try to update the items (if they already exist).
	        $.each( arrOfLineItems, function( index, val ) {
	          
				var item = val;
	            var itemId = '#variant-id-' + item.variant_id;
	          	
				// if html element already exists
				// update view
				if ( $(itemId).length ) {
	              
					var variantID = item.variant_id;
					var itemId = '#variant-id-' + variantID;
					var variantQty = item.quantity;
	              
					$(itemId  + " input.cart-list-item__quantity").val( variantQty );
					$(itemId  + " .product-quantity-list-item--quantity").html( variantQty );
	                itemsUpdated[itemId] = true;

				}
	
	        }); // each cart line item
	      
			//  This loop *only* adds the item to the list if it was not present (or updated) before.
	        $.each( arrOfLineItems, function( index, val ){
	
	            var item = val;
	            var itemId = '#variant-id-' + item.variant_id;
	            var html;
	
	            if ( itemsUpdated[itemId] != true ) {
	                    
					// Get vars from 'item'
					var variantID = item.variant_id;
	                var productType = item.product_type;
	                var productTitle = item.product_title;
	                var variantTitle = item.variant_title;
					var variantQty = item.quantity;
					var variantPrice = ( item.price / 100 ).toFixed(2);
					var variantURL = item.url;
					var imageSRC = item.image;
	                            
	              	// Check 'imageSRC' against array of image file types
	              	// If valid image file type
	              	// String replace for smaller image to optimize experience
	                $.each( imageFileTypes, function(index, value){
						if ( imageSRC.indexOf( value ) !== -1 ) {
							// imageSRC string HAS proper image file type extension
	                        // replace string with 'small' version of image for optimization
	                        imageSRC = imageSRC.replace( value, "_small" + value ); 
	                        return false; 
	                   	}                      
	                });
	              
					// Template HTML and add to 'push cart list' element
	                html = '';
	                  
	                html += "<li id='variant-id-" + variantID + "' class='push-cart-list-item' data-variant-id='" + variantID + "'>";
	                  
	                html += "<div class='push-cart-list-item__image'>";
	                html += "<a href='" + variantURL + "'>";
					html += "<img src='" + imageSRC + "' alt='" + productTitle + "'/>";
	                html += "</a>";
					html += "</div>";
	                  
	                html += "<div class='push-cart-list-item__info'>";
	                html += "<h2 class='product-title'>" + productTitle + "</h2>";
					html += "<div class='product-options product-options--quantity'>";
					
					
					if ( variantTitle != null ) {
						html += "<span class='product-variant product-variant--title'>" + variantTitle + "</span>";
					}
					
					html += "<ul class='product-quantity-list'>";
					html += "<li class='product-quantity-list-item product-quantity-list-item--decrease decrease js--update-cart'>";
					html += "<img src='" + iconQtyDecrease + "' alt='Decrease Quantity'/>";
					html += "</li>";
					html += "<li class='product-quantity-list-item product-quantity-list-item--quantity quantity'>" + variantQty + "</li>";
					html += "<li class='product-quantity-list-item product-quantity-list-item--increase increase js--update-cart'>";
					html += "<img src='" + iconQtyIncrease + "' alt='Increase Quantity'/>";
					html += "</li>";
					html += "</ul>";
					html += "</div>";
					html += "<div class='product-options product-options--price'>";      
					html += "<span class='product-variant product-variant--price'>$" + variantPrice + " " + shopCurrency + "</span>";
					html += "<ul class='product-quantity-list'>";
					html += "<li class='product-quantity-list-item product-quantity-list-item--remove remove js--update-cart'>Remove</li>";
					html += "</ul>";
					html += "</div>";
					html += "</div>";
					
					html += "<input style='display:none;' class='cart-list-item__quantity' type='number' name='updates[]' id='updates_" + variantID + "' value='" + variantQty + "' />";
	                  
					html += "</li>";
	
	                $("#push-cart-list").prepend(html);	
		
	            } // end if
	
	        }); // end cart item	
		
			// Cart has at least 1 item
			$("#push-content--overlay-left .push-cart-empty-message").fadeOut(320);
	    
	    } else {
	    
			// Cart is empty
			setTimeout(function(){
				$("#push-content--overlay-left .push-cart-empty-message").fadeIn(650);
	        }, 1250);	
	      
	    }
	
	};
	
	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////
	
	return {
		info : info,
		init : init,
		update : update
	};

})();