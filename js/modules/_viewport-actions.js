//////////////////////////////////////////////////////////
////  Viewport Offset
//////////////////////////////////////////////////////////

var ViewportActions = (function () {

	var debugThis = false;
	var info = { name : 'ViewportActions', version : 1.0 };
	
	var breakpoints = {
		'xs' : 567,
		'sm' : 768,	
		'md' : 992,
		'lg' : 1200,
		'xlg' : 1400
	};
	
	//////////////////////////////////////////////////////////
	////  Public | Offset from Top
	//////////////////////////////////////////////////////////
	
	var OffsetFromTop = function () {
		
		setState();
		
		Viewport.onScroll( setState, 0 );
		
		function setState() {
			
			var elements = [
				{ target : 'header', range : 5 }
			];
			
			if ( elements.length ) {
				
				var scroll = $(window).scrollTop();
	
				$.each( elements, function( index, element ) {
			
					var thisElement = $( element.target );
									
					if ( scroll >= element.range ) {
						thisElement.removeClass('at-top').addClass("offset-from-top");
					} else {
						thisElement.removeClass("offset-from-top").addClass('at-top');
					}    
				
				});
				
			}	
			
		};
		
	};
	
	//////////////////////////////////////////////////////////
	////  Public Method | Element Height Setter
	//////////////////////////////////////////////////////////
	
	var matchViewportHeight = function () {
			
		setHeight();	
		
		Viewport.onResize( setHeight, 150 );
	
		function setHeight() {
										
			if ( $('.js--match-viewport-height').length ) {
				
				var target = $('.js--match-viewport-height');
								
				var viewportHeight = window.innerHeight;
				var viewportWidth = window.innerWidth;
				var mobileHeight = screen.availHeight;
				
				if ( target.hasClass('js--match-viewport-height--header-offset') ) {
					viewportHeight = viewportHeight - $('header').outerHeight();
				}
				
				if ( target.hasClass('js--match-viewport-height--two-thirds') ) {
					viewportHeight = viewportHeight * 0.6666;
				}
				
				if ( target.hasClass('js--match-viewport-height--three-quarters') ) {
					viewportHeight = viewportHeight * 0.75;
				}
								
				if ( target.hasClass('js--match-viewport-height--desktop-only') ) {
					
					// run it only on desktop only, ie 'md' breakpoint and up
										
					if ( viewportWidth >= breakpoints.md ) {
						target.css({
							'height' : viewportHeight + 'px'
						});
					} else {
						target.css({
							'height' : 'auto'
						});
					}
					
				} else {
					
					// run it every time
					
					target.css({
						'height' : viewportHeight + 'px'
					});
					
				}
								
			}
			
		};	
		
	};
	
	//////////////////////////////////////////////////////////
	////  Public Method | Element Height Setter
	//////////////////////////////////////////////////////////
	
	var elementHeightSetter = function() {
				
		setHeight();
		
		Viewport.onResize( setHeight, 150 );
		
		function setHeight() {
					
			var vph = window.innerHeight;
			var vpw = window.innerWidth;
			var hh = $('header').outerHeight();
			var fh = $('footer').outerHeight();
			var target = $(".js--set-height");
			var mobileSetHeight = false;
			var offsetHeaderHeight = false;
			var offsetFooterHeight = false;
			
			target.each(function(){
			
				var thisEl = $(this);
				var childEl = false;
				
				if ( thisEl.hasClass('collection__images') ) {
					childEl = thisEl.find('.carousel__item');
				}
				
				// enbale set height on mobile? 
				if ( thisEl.attr('data-set-height-mobile') ) {
					mobileSetHeight = ( thisEl.attr('data-set-height-mobile') == 'true' );
				}
				
				// offset header height from viewport height?
				if ( thisEl.attr('data-set-height-header-offset') ) {
					offsetHeaderHeight = ( thisEl.attr('data-set-height-header-offset') == 'true' );
					if ( offsetHeaderHeight ) {
						vph = vph - hh;
					}
				}
				
				// offset header height from viewport height?
				if ( thisEl.attr('data-set-height-footer-offset') ) {
					offsetHeaderHeight = ( thisEl.attr('data-set-height-footer-offset') == 'true' );
					if ( offsetHeaderHeight ) {
						vph = vph - fh;
					}
				}
				
				// set element height based on earlier conditions					
				if ( mobileSetHeight ) {
					
					target.css({
						'height' : vph + 'px'
					});
					
					if ( childEl ) {
						childEl.css({
							'height' : vph + 'px'
						});
					}
					
				} else {
					
					if ( vpw >= breakpoints.md ) {
						target.css({
							'height' : vph + 'px'
						});
						if ( childEl ) {
							childEl.css({
								'height' : vph + 'px'
							});
						}
					} else {
						target.css({
							'height' : 'auto'
						});
						if ( childEl ) {
							childEl.attr('style', '');
						}
					}
					
				}
							
			});
		
		}	
						
	};
	
	//////////////////////////////////////////////////////////
	////  Public Method | Element Width Setter
	//////////////////////////////////////////////////////////
	
	var elementWidthSetter = function() {
				
		setWidth();
		
		Viewport.onResize( setWidth, 150 );
			
		function setWidth() {
					
			var vph = window.innerHeight;
			var vpw = window.innerWidth;
			
			var target = $(".js--set-width");
			var onMobile = false;
			var widthPercent = 0;
			var offsetHeaderHeight = false;
			var offsetFooterHeight = false;
			
			var newWidth = 0;
			
			target.each(function(){
			
				var thisEl = $(this);
				
				// enbale set height on mobile? 
				if ( thisEl.attr('data-set-width-mobile') ) {
					onMobile = ( thisEl.attr('data-set-width-mobile') == 'true' );
				}
				
				// offset header height from viewport height?
				if ( thisEl.attr('data-set-width-percent') ) {
					widthPercent = parseFloat( thisEl.attr('data-set-width-percent') );
				}
				
				newWidth = (widthPercent * vpw) - 20
				
				// set element height based on earlier conditions					
				if ( onMobile ) {
					
					target.css({
						'width' : newWidth + 'px'
					});
										
				} else {
					
					if ( vpw >= breakpoints.md ) {
						target.css({
							'width' : newWidth + 'px'
						});
						
					} else {
						
						target.attr( 'style', '' );
						
					}
					
				}
							
			});
		
		}	
						
	};
	
	//////////////////////////////////////////////////////////
	////  Public Method | Element Height Setter
	//////////////////////////////////////////////////////////
	
	var elementVerticalPositioning = function() {
				
		setPosition();
		
		Viewport.onResize( setPosition, 150 );
				
		function setPosition() {
							
			var vph = window.innerHeight;
			var vpw = window.innerWidth;
			var hh = $('header').outerHeight();
			var fh = $('footer').outerHeight();	
			
			console.log( 'viewport inner height:' );
			console.log( vph );	
							
			$(".js--vertical-positioning").each(function(){

				// this element				
				var thisEl = $(this);
				var thisElHeight = thisEl.outerHeight();
				
				//console.log( 'footer and header heights:' );
				//console.log( [ { thisEl : thisElHeight }, { footer : footerHeight }, { header : headerHeight }, { total : totalHeight } ] );
											
				var availableHeight = ( vph - ( fh + hh ) );
				var verticalSpacing = ( availableHeight - thisElHeight ) / 2;
				
				console.log( [ { avail : availableHeight }, { spacing : verticalSpacing } ] );
				
				if ( vpw >= breakpoints.md ) {
					thisEl.css({
						'top' : verticalSpacing + 'px'
					});
				} else {
					thisEl.attr('style','');
				}
				
			});
		
		}	
						
	};
	
	//////////////////////////////////////////////////////////
	////  Public Method | Element Height Setter
	//////////////////////////////////////////////////////////
	
	var elementHorizontalPositioning = function() {
				
		setPosition();
		
		Viewport.onResize( setPosition, 150 );
				
		function setPosition() {
							
			var vph = window.innerHeight;
			var vpw = window.innerWidth;
							
			$(".js--horizontal-positioning").each(function(){

				// this element				
				var thisEl = $(this);
				var thisElOffset = thisEl.offset();
				var onMobile = false;
				var alignment = 'left';
				var newPosition = 0;
				var cssObj = {};
				
				if ( thisEl.attr('data-horizontal-position-mobile') ) {
					onMobile = ( thisEl.attr('data-horizontal-position-mobile') == 'true' );
				}
				
				// offset header height from viewport height?
				if ( thisEl.attr('data-horizontal-alignment') ) {
					alignment = thisEl.attr('data-horizontal-alignment');
				}
				
				switch ( alignment ) {
					case 'left':	
						newPosition = ( -1 * thisElOffset.left );	
						cssObj = { 'left' : newPosition + 'px' };
						break;
					case 'right':	
						newPosition = 10;	
						cssObj = { 'left' : newPosition + 'px' };				
						break;	
					default:
						break;
				}
				
				
					if ( vpw >= breakpoints.md ) {
						thisEl.css(cssObj);
					} else {
						thisEl.attr('style','');
					}
					
							
			});
		
		}	
						
	};
			
	//////////////////////////////////////////////////////////
	////  Public Methods
	//////////////////////////////////////////////////////////

	var init = function( $params ) {
		
		Debug.log( debugThis, info, 'start' );
		
		OffsetFromTop();
		
		matchViewportHeight();

		Debug.log( debugThis, info, 'stop' );		
			
	};
	
	//////////////////////////////////////////////////////////
	////  Returned Methods
	//////////////////////////////////////////////////////////
	
	return {
		info : info,
		init : init
	};

})();